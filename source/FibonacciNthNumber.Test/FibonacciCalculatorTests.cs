﻿using FibonacciNthNumber.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FibonacciNthNumber.Test
{
    [TestClass]
    public class FibonacciCalculatorTests
    {
        private FibonacciCalculator _fibonacciCalculator;

        [TestInitialize]
        public void Setup()
        {
            _fibonacciCalculator = new FibonacciCalculator();
        }

        [TestMethod]
        public void GetFibonacciNum_GivenInput_1_ReturnsCorrectValue()
        {
            // Arrange
            var inputNum = "1";
            var expectedValue = "1";

            // Act
            var result = _fibonacciCalculator.GetFibonacciNumber(inputNum);

            // Assert
            Assert.AreEqual(expectedValue, result, $"Expected: {expectedValue}, actual: {result}");
        }

        [TestMethod]
        public void GetFibonacciNum_GivenInput_14_ReturnsCorrectValue()
        {
            // Arrange
            var inputNum = "14";
            var expectedValue = "377";

            // Act
            var result = _fibonacciCalculator.GetFibonacciNumber(inputNum);

            // Assert
            Assert.AreEqual(expectedValue, result, $"Expected: {expectedValue}, actual: {result}");
        }

        [TestMethod]
        public void GetFibonacciNum_GivenInput_78_ReturnsCorrectValue()
        {
            // Arrange
            var inputNum = "78";
            var expectedValue = "8,944,394,323,791,460";

            // Act
            var result = _fibonacciCalculator.GetFibonacciNumber(inputNum);

            // Assert
            Assert.AreEqual(expectedValue, result, $"Expected: {expectedValue}, actual: {result}");
        }

        [TestMethod]
        public void GetFibonacciNum_GivenNegativeInput_ReturnsEmptyValue()
        {
            // Arrange
            var inputNum = "-1";
            var expectedValue = string.Empty;

            // Act
            var result = _fibonacciCalculator.GetFibonacciNumber(inputNum);

            // Assert
            Assert.AreEqual(expectedValue, result, $"Expected: {expectedValue}, actual: {result}");
        }
    }
}
