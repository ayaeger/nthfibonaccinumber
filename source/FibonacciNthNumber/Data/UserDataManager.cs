﻿using System;
using System.Collections.Generic;
using FibonacciNthNumber.Utilities;
using System.Linq;

namespace FibonacciNthNumber.Data
{
    public interface IUserDataManager
    {
        IFibonacciCalculator FibonacciCalculatorDI { get; set; }
        void CreateNewEntry(string n, string userIP);
        IEnumerable<UserData> GetNumEntries(int numEntries);
    }

    public class UserDataManager : IUserDataManager
    {
        public IFibonacciCalculator FibonacciCalculatorDI { get; set; } = new FibonacciCalculator();
        private FibonacciNthNumber20170702020828_dbEntities _context = new FibonacciNthNumber20170702020828_dbEntities();

        public void CreateNewEntry(string n, string userIP)
        {
            // calculate the fibonacci number
            var fibonacciResult = FibonacciCalculatorDI.GetFibonacciNumber(n);

            // if the calculation was unsuccessful based on the input, just return
            if (string.IsNullOrEmpty(fibonacciResult)) return;

            // otherwise create a new user data record using our calculation
            var userData = new UserData
            {
                SourceNum = int.Parse(n),
                FibonacciNum = fibonacciResult,
                UseDate = DateTime.Now.ToString(),
                UserIP = userIP
            };
            _context.UserDatas.Add(userData);
            _context.SaveChanges();
        }

        public IEnumerable<UserData> GetNumEntries(int numEntries)
        {
            var allEntries = _context.UserDatas.ToList();
            return allEntries.OrderByDescending(e => DateTime.Parse(e.UseDate)).Take(numEntries);
        }
    }
}