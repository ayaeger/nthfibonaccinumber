﻿using System;
using System.Web.Mvc;
using FibonacciNthNumber.Data;

namespace FibonacciNthNumber.Controllers
{
    public class HomeController : Controller
    {
        public IUserDataManager UserDataManagerDI { get; set; } = new UserDataManager();

        public ActionResult Index()
        {
            return View();
        }

        /* 
        /Home/CalculateNewFibNumber 
        */
        public JsonResult CalculateNewFibNumber(string n)
        {
            try
            {
                // create a new db entry for this request
                UserDataManagerDI.CreateNewEntry(n, Request.UserHostAddress);

                // get the ten most recent calculations
                var lastTenEntries = UserDataManagerDI.GetNumEntries(10);

                return Json(lastTenEntries, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}