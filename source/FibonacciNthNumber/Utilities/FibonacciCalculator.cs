﻿using System.Linq;

namespace FibonacciNthNumber.Utilities
{
    public interface IFibonacciCalculator
    {
        string GetFibonacciNumber(string n);
    }

    public class FibonacciCalculator : IFibonacciCalculator
    {
        public string GetFibonacciNumber(string n)
        {
            int sourceNum;
            if (int.TryParse(n, out sourceNum))
            {
                // we won't try to calculate anything negative
                if (sourceNum < 0) return string.Empty;

                // get the fibonacci num - using double to handle large nums
                double fib = CalculateFibonacci(sourceNum);
                var fullNum = fib.ToString("N");
                return fullNum.Split('.').First();
            }

            // if the parse input failed, return empty string
            return string.Empty;
        }

        private double CalculateFibonacci(int n)
        {
            double firstNum = 0;
            double secondNum = 1;
            double fibonacciNum = 0;

            // if the input is 1, just return 1
            if (n == 1) fibonacciNum = secondNum;

            // otherwise, iterate though building the sequence
            else
            {
                var sequenceNum = 1;
                while (sequenceNum < n)
                {
                    fibonacciNum = firstNum + secondNum;
                    firstNum = secondNum;
                    secondNum = fibonacciNum;
                    ++sequenceNum;
                }
            }

            return fibonacciNum;
        }
    }
}