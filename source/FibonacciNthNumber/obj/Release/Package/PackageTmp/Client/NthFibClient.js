﻿var app = angular.module('NthFibApp', ['ngAnimate', 'angularMoment']);

// angular service to hit web api
app.factory('remoteService', ['$http', function ($http) {
    var remoteService = {};
    remoteService.GetUserData = function (num) {
        var params = { params: { n: num } }
        return $http.get('/Home/CalculateNewFibNumber', params);
    };
    return remoteService;
}]);

app.controller('NthFibController', ['$scope', 'remoteService', 'moment', function ($scope, remoteService, moment) {

    $scope.newNthNumber = null;
    $scope.fibonacciResult = '';
    $scope.showResult = false;

    // initialize
    getUserData();

    // method to calculate new fibonacci num and load results
    function getUserData() {
        remoteService.GetUserData($scope.newNthNumber)
            .then(function (userData) {
            if (userData.data) {
                // load the backing collection for the table
                $scope.userData = userData.data;
                // load the result for this calculation
                if ($scope.showResult) $scope.fibonacciResult = 'Result: ' + $scope.userData[0].FibonacciNum;
            }
            }, function (err) {
                console.log('Error: statusCode = ' + err.status + ' statusText = ' + err.statusText)
            });
    }

    // click event handler for calculate button
    $scope.calculate = function () {
        console.log('Calculating new fib number: ' + $scope.newNthNumber);
        $scope.showResult = true;
        getUserData();        
        $scope.newNthNumber = null;
    }
}]);