# README #

This is a simple ASP.NET MVC 5 web application built to fulfill the requirements outlined in the N-th Fibonacci Number project document.  

The application allows a user to input a number (n) and displays the n-th number of the Fibonacci Sequence.  Additionally, the app shows data from the 10 most recent calculations.

### Tools/Frameworks ###

* ASP.NET MVC 5
* AngularJS
* Bootstrap
* SQL Server

### Design Decisions ###

The MVC 5 framework offers a clean way to bundle and serve the JavaScript and HTML/CSS files, as well as set up a data layer using a SQL Server and ADO.Net.  Additionally, creating an MVC application in Visual Studio allows a straightforward way to host the site and database on Azure.

Angular/Bootstrap for the Javascript portion of the web app allowed me to utilize clean styling and an organized, responsive web page.  Additionally, Angular is convenient for easily injecting dependencies and communicating with the Web API service.

### Notable Problem Solutions ###
* The resulting Fibonacci numbers in the sequence get very large very quickly.  Because of this, the standard 32-bit integer was not large enough to go very far into the Fibonacci sequence.  To handle larger numbers, I used a double for calculating the n-th number, and stored the result as a string formatted the way I wanted it (varchar).
* Another potential issue was invalid user input for n when calculating the n-th Fibonacci number.  Obviously, negative numbers are not valid as well as numbers that are just too large.  To handle this, I limited input to numbers greater than zero and less than 100 (I could have gone larger than 100, but this seemed to meet the intent of the project).  Validation in the HTML only allows numbers, and has a min value and max size.  Additionally, the user cannot begin a calculation until inputting a valid number.  Finally, the Web API service checks the input number and will not attempt to calculate an invalid entry.
* There were other small issues to handle, but not especially noteworthy.